import { sanityClient } from '@/sanity';
import VariantView from '@/src/views/VariantView'
import { GetStaticProps } from 'next';
import React from 'react'

interface Props{
    variant: any
}

function Variant({variant}:Props) {

    // console.log(product)

    return (
        <VariantView
            variant={variant}
        />
    )
}

export default Variant

export async function getStaticPaths({locales}: any) {

    const query = `*[_type == "variant" && id!=null]{
      _id,
      slug {
          current
      }
  } `;

    const variants = await sanityClient.fetch(query);

    const paths = variants
        .map((variant: any ) =>({

                params: { slug: variant?.id },
                // locale, //locale should not be inside `params`
            })
        )
        .flat();

    return {
        paths,
        fallback: false
    }
}

export const getStaticProps: GetStaticProps = async ({params}:any) => {

    const variantQuery= `*[_type == "variant" && id = $id ][0] {
    _id,
    _createdAt,
    name,
    slug,
    mainImage,
    price,
    description,
    category->{
      name
    }
  
  }`

    const variant = await sanityClient.fetch(variantQuery, {
        slug: params?.slug,
    })

    if(!variant){
        return {
            notFound: true
        }
    }

    return {
        props: {
            variant,
        },
        revalidate: 10
    };
};