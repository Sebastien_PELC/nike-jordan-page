import React from 'react'
import Layout from '../components/Layouts/Layout'
import VariantBottomSection from "@/src/components/Variants/VariantBottomSection";

interface Props{
    variant: any
}

function VariantView({variant}:Props) {

    return (
        <Layout>
            <VariantBottomSection
                variant={variant}
            />
        </Layout>
    )
}

export default VariantView
