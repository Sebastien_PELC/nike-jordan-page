import React from 'react'
import VariantMainImage from './VariantMainImage'
import VariantInfo from './VariantInfo'
import { urlFor } from '@/sanity'

interface Props{
    variant: any
}

function VariantBottomSection({variant}:Props) {
    return (
        <section
            className='flex w-full gap-10 p-14 justify-center'
        >
            <VariantMainImage
                imageUrl={variant?.mainImage ? urlFor(variant?.mainImage).url()! : `/Images/hado.jpg`}
            />
            <VariantInfo
                name={variant?.name}
                category={variant?.category?.name}
                description={variant?.description}
                price={variant?.price}
            />
        </section>
    )
}

export default VariantBottomSection
