import React from 'react'
import Image from "next/image";
import SelectComponent from "@/src/components/Jordan/SelectComponent";
import ButtonComponent from "@/src/components/Jordan/ButtonComponent";

function CardComponent() {
    return (
        <div
            className={`w-[70vw] h-[90vh] relative bg-[#555863] opacity-80 z-20 rounded-3xl`}
        >
            <div
            className={`flex flex-col gap-5`}
            >
                <div
                className={`relative`}
                >
                    <h2
                        className={`mx-auto w-full text-white opacity-10 font-bold text-[10rem] text-center`}
                    >FLYKNIT</h2>
                    <h3
                    className={`absolute top-[40%] right-[15%] z-30 text-6xl font-bold bg-gradient-to-t from-[#555761] to-[#FFFFFF] text-transparent bg-clip-text`}
                    >FLYKNIT</h3>
                    <h4
                        className={`absolute top-[25%] right-[35%] z-30 text-2xl font-bold shadow-2xl bg-gradient-to-r from-[#E99450] to-[#E37D4A] text-transparent bg-clip-text`}
                    >NIKE</h4>
                </div>
                <div
                className={`self-end w-3/5`}
                >
                    <h3
                    className={`text-white opacity-80 font-bold text-xl my-3 shadow-2xl`}
                    >PRODUCT OVERVIEW</h3>
                    <div>
                        <p
                        className={`pl-12 my-5 opacity-40 text-white`}
                        >Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid dolorum excepturi, quaerat rem sit voluptatibus?</p>
                        <div
                        className={`flex gap-3 pl-12`}
                        >
                            <SelectComponent
                            selected={false}
                            value={`7`}
                            />
                            <SelectComponent
                               selected={false}
                                value={`8`}
                            />
                            <SelectComponent
                                selected={true}
                                value={`9`}
                            />
                            <SelectComponent
                                selected={false}
                                value={`10`}
                            />
                        </div>
                    </div>
                </div>
                <div
                className={`self-end flex text-white w-3/5 justify-around my-3 items-center`}
                >
                    <p
                    className={`text-2xl font-bold text-center pl-12 shadow-2xl`}
                    >$120</p>
                    <ButtonComponent/>
                </div>

            </div>

            <div
                className={`absolute top-[-10%] left-[-10%]`}
            >
                <Image
                    src={`/Images/jordan.png`}
                    alt="Chaussure Jordan"
                    width={700}
                    height={700}
                    className="object-cover rotate-45 drop-shadow-2xl"
                ></Image>
            </div>
        </div>

    )
}

export default CardComponent
