import React from 'react'
import Image from "next/image";
import CardComponent from "@/src/components/Jordan/CardComponent";

function BackGroundComponent() {
    return (
        <div
            className={`w-full h-full`}
        >
            <div
                className={`absolute z-10 top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%]`}
            >
                <CardComponent/>
            </div>
            <div>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
                    <path fill="#3F444E" fill-opacity="1"
                          d="M0,0L40,37.3C80,75,160,149,240,197.3C320,245,400,267,480,245.3C560,224,640,160,720,160C800,160,880,224,960,256C1040,288,1120,288,1200,282.7C1280,277,1360,267,1400,261.3L1440,256L1440,320L1400,320C1360,320,1280,320,1200,320C1120,320,1040,320,960,320C880,320,800,320,720,320C640,320,560,320,480,320C400,320,320,320,240,320C160,320,80,320,40,320L0,320Z"></path>
                </svg>
            </div>
            <div
                className={`w-full h-[100vh] bg-[#3F444E]`}
            ></div>
        </div>
    )
}

export default BackGroundComponent
