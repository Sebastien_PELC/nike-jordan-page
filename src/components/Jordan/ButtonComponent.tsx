import React from 'react'
function ButtonComponent() {
    return (
       <div
       className={`w-1/3 py-3 bg-gradient-to-r from-[#E99450] to-[#E37D4A] flex justify-center items-center rounded-xl font-bold shadow-2xl`}
       >
           <h3>PURCHASE</h3>
       </div>
    )
}

export default ButtonComponent
