import React from 'react'

interface Props{
    selected: boolean,
    value: string
}
function SelectComponent({selected, value} : Props) {
    return (
       <div
       className={`${selected ? `bg-[#E37D4A]` : `bg-[#40434F]`} w-[50px] h-[50px] flex justify-center items-center text-white text-bold rounded shadow-2xl`}
       >
           <span
           className={`text-bold`}
           >{value}</span>
       </div>
    )
}

export default SelectComponent
