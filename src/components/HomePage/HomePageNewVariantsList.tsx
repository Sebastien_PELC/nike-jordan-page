import React from 'react'
import HomePageNewProductListItem from './HomePageNewProductListItem'
import { urlFor } from '@/sanity'

interface Props{
  variants: any

}

function HomePageNewVariantsList({variants}:Props) {
  console.log('Our Variants:', variants)
  return (
    <ul
    className=' flex space-x-4 overflow-x-auto '
    >
      {
        variants && variants.length > 0
        ?
        variants.map((variant:any) =>
        <HomePageNewProductListItem
        key={variant?._id}
      imageUrl={variant?.pictures ? urlFor(variant?.mainImage).url()! : `/Images/hado.jpg`}
      name={variant?.name}
      description={variant?.category?.name}
      price={variant?.price}
      />
        )
        :
        <span>
          {`No variant yet`}
        </span>
      }
      
      
    </ul>
  )
}

export default HomePageNewVariantsList
